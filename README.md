# HTML y CSS Básico 🚀

_Curso para construir contenido Web bien estructurado y fácil de mantener al conservar etiquetas y reglas de estilo en orden, pero sobre todo pensando en el trabajo colaborativo._

## Autor ✒️

* **Manuel Nava R.** - *Trabajo Inicial* - [manuuelnrs](https://gitlab.com/manuuelnrs)
* **Edgar Vargas** - *Mentor*